# Multiple Map Pins

This code is an example of how to draw multiple location pins from a single json file.

It also taks advantage of HTML5 geolocation.

This was based on a one page website I developed to show locations of several pubs.

Each pub is listed in a json array inside js/pubs.js. Every item has the address, phone number, co-ordinates and other attributes availible.

Inside multi-pin-map.js, I load the json and using the other functions, draw the markers and pop up boxes.

Custom icon is there to show the code required.