// Setting variables for later user
var icon;
var beericon = new google.maps.MarkerImage(
	"assets/beer-ico.png",
	new google.maps.Size(45, 40),
	new google.maps.Point(0, 0),
	new google.maps.Point(17, 40)
);
var center = null;
var map = null;
var currentPopup;
var bounds = new google.maps.LatLngBounds();
var html;

// Has position is used for geolocation
function hasPosition(position) {
	var points = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
	latty = points.Ya;
	longy = points.Za;

	// passes co-ordinates to add marker function to create a 'you' marker. Passing true as a type so it doesn't get the custom icon
	addMarker(latty, longy, 'you', true);
}

// Function to add the new marker on the map.
function addMarker(lat, lng, info, type) {
	var pt = new google.maps.LatLng(lat, lng);
	
	if(!type)
		icon = beericon;

	var marker = new google.maps.Marker({
		position: pt,
		icon: icon,
		map: map
	});
	var popup = new google.maps.InfoWindow({
		content: info,
		maxWidth: 320
	});

	// Closes any open pop ups before opening the required one
	google.maps.event.addListener(marker, "click", function() {
		if (currentPopup != null) {
			currentPopup.close();
			currentPopup = null;
		}
		popup.open(map, marker);
		currentPopup = popup;
	});

	// Closes the popup when the X is closed
	google.maps.event.addListener(popup, "closeclick", function() {
		currentPopup = null;
	});

	icon = null;
}

// This function just seperates out the HTML for the pop up to allow for easy editing
function formatHtml(blurb, address, tel, website) {
	var websitetext;
	if(website) {
		websitetext = '<div class="website"><a href="' + website + '">Visit the Website</a></div>';
	} else {
		websitetext = '';
	}
	var html = 	'<div class="pubdets">' +
					'<div class="pubname">' + blurb + '</div>' +
					'<div class="address">' + address + '</div>' +
					'<div class="tel">Tel: ' + tel + '</div>' +
					websitetext +
				'</div>';
    return html;
}

// This is the heavy lifter - grabs the feed then iterates over the array, passing details through to the formatHTML function before passing to the addMarker function
function getData() {
	$.getJSON("js/pubs.js", function (data) {
		$.each(data, function(name, details){
			html = formatHtml(details.name, details.address, details.phone, details.website);
			addMarker(details.lat, details.long, html);
		});
	});
}

// This is what initializes the map, then instigates getting the data (so that the map is there before making markers) and then lastly ansking and creating the geolocated marker
$(document).ready(function() {
	map = new google.maps.Map(document.getElementById("map"), {
		center: new google.maps.LatLng(50.907938, -0.105443),
		zoom: 10,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	});
	getData();
    if(navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(hasPosition);
    }
});