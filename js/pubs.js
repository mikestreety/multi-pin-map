{
	"pub1": {
		"name": "Royal Oak",
		"address": "High Street<br />Barcombe<br />Sussex<br />BN8 5BA",
		"phone": "01273 400418",
		"website": "www.royaloakbarcombe.co.uk",
		"lat": "50.923949",
		"long": "0.019419"
	},
	"pub2": {
		"name": "Basketmakers Arms",
		"address": "12 Gloucester Rd, <br />Brighton, <br />BN1 4AB",
		"phone": "01273 689006",
		"website": "www.thebasketmakersarms.co.uk",
		"lat": "50.8266",
		"long": "-0.136771"
	},
	"pub3": {
		"name": "Battle of Trafalgar",
		"address": "34 Guildford St,<br /> Brighton, <br />BN1 3LW",
		"phone": "01273 327997",
		"lat": "50.828818",
		"long": "-0.142525"
	},
	"pub4": {
		"name": "Evening Star",
		"address": "56 Surrey St, <br />Brighton, <br />BN1 3PB",
		"phone": "01273 328931",
		"website": "www.eveningstarbrighton.co.uk",
		"lat": "50.8276",
		"long": "-0.142213"
	},
	"pub5": {
		"name": "Greys",
		"address": "105 Southover St, <br />Brighton, <br />BN2 9UA",
		"phone": "01273 680734",
		"website": "www.greyspub.com",
		"lat": "50.8292",
		"long": "-0.13044"
	},
	"pub6": {
		"name": "Lord Nelson Inn",
		"address": "36 Trafalgar St, <br />Brighton, <br />BN1 4ED",
		"phone": "01273 695872",
		"website": "www.thelordnelsoninn.co.uk",
		"lat": "50.8284",
		"long": "-0.13923"
	},
	"pub7": {
		"name": "Mitre",
		"address": "13 Baker St, <br />Brighton, <br />BN1 4JN",
		"phone": "01273 683173",
		"website": "www.mitretavern.co.uk",
		"lat": "50.831803",
		"long": "-0.135352"
	},
	"pub8": {
		"name": "Prestonville Arms",
		"address": "64 Hamilton Rd, <br />Brighton, <br />BN1 5DN",
		"phone": "01273 701 007",
		"website": "www.theprestonvillearms.co.uk",
		"lat": "50.8337",
		"long": "-0.14528"
	},
	"pub9": {
		"name": "Pump House",
		"address": "46 Market Street,<br />Brighton,<br />BN1 1HH",
		"phone": "01273 827421",
		"website": "www.nicholsonspubs.co.uk/thepumphousebrighton",
		"lat": "50.821581",
		"long": "-0.140046"
	},
	"pub10": {
		"name": "Sir Charles Napier",
		"address": "50 Southover St, <br />Brighton, <br />BN2 2UE",
		"phone": "01273 601413",
		"website": "www.thesircharlesnapier.co.uk",
		"lat": "50.8278",
		"long": "-0.128029"
	},
	"pub11": {
		"name": "Victory Inn",
		"address": "6 Duke Street<br />Brighton,<br />East Sussex<br />BN1 1AH",
		"phone": "01273 326555",
		"lat": "50.822597",
		"long": "-0.142608"
	},
	"pub12": {
		"name": "Ship Inn",
		"address": "Whitemans Green, <br />Cuckfield, <br />West Sussex<br />RH17 5BY",
		"phone": "01444 413219 ",
		"lat": "51.014140",
		"long": "-0.142610"
	},
	"pub13": {
		"name": "Swan Inn",
		"address": "Middle Street<br />Falmer,<br />East Sussex<br />BN1 9PD",
		"phone": "01273 681842",
		"lat": "50.863868",
		"long": "-0.078111"
	},
	"pub14": {
		"name": "Pig and Butcher",
		"address": "Five Ash Down, <br />Buxted, <br />East Sussex<br>TN22 3AN",
		"phone": "01825 732191",
		"website": "www.harveys.org.uk/thepigandbutcherfiveashdown.php",
		"lat": "50.994881",
		"long": "0.103318"
	},
	"pub15": {
		"name": "Shepherd &amp; Dog",
		"address": "The Street, <br />Fulking, <br>Henfield, <br>West Sussex,<br>BN5 9LU",
		"phone": "01273 857382",
		"website": "shepherdanddogpub.co.uk",
		"lat": "50.888121",
		"long": "-0.228907"
	},
	"pub16": {
		"name": "Neptune Inn",
		"address": "10 Victoria Terrace,<br>Kingsway,<br>Hove,BN3 2WB",
		"phone": "01273 736390",
		"website": "www.theneptunelivemusicbar.co.uk",
		"lat": "50.825146",
		"long": "-0.175948"
	},
	"pub17": {
		"name": "Poacher",
		"address": "139 High Street,<br>Hurstpierpoint,<br>Hassocks,<br>West Sussex,<br>BN6 9PU",
		"phone": "01273 834893",
		"lat": "50.932369",
		"long": "-0.174627"
	},
	"pub18": {
		"name": "Laughing Fish",
		"address": "Station Road,<br>Isfield,<br>Nr. Uckfield,<br>East Sussex,<br>TN22 5XB",
		"phone": "01825 750349",
		"website": "www.laughingfishonline.co.uk",
		"lat": "50.9351",
		"long": "0.06546"
	},
	"pub19": {
		"name": "Brewers Arms",
		"address": "91 High St, <br>Lewes, <br>East Sussex, <br>BN7 1XN",
		"phone": "01273 475524",
		"website": "www.brewersarmslewes.co.uk",
		"lat": "50.871181",
		"long": "0.008197"
	},
	"pub20": {
		"name": "Constitutional Club",
		"address": "139 High Street,<br>Lewes,<br>East Sussex<br>BN7 1XS",
		"phone": "01273 473076 ",
		"website": "www.lewesconclub.com",
		"lat": "50.872151",
		"long": "0.005118"
	},
	"pub21": {
		"name": "Elephant and Castle",
		"address": "White Hill, <br />Lewes, <br />East Sussex BN7 2DJ",
		"phone": "01273 473797",
		"website": "www.elephantandcastlelewes.com",
		"lat": "50.874634",
		"long": "0.008712"
	},
	"pub22": {
		"name": "Gardeners Arms",
		"address": "46 Cliffe High St, <br />Lewes, <br />East Sussex BN7 2AN",
		"phone": "01273 474808 ",
		"lat": "50.8742",
		"long": "0.0175485"
	},
	"pub23": {
		"name": "John Harvey Tavern",
		"address": "6 Cliffe High St, <br />Lewes, <br />East Sussex BN7 2AN",
		"phone": "01273 479880 ",
		"website": "www.johnharveytavern.co.uk",
		"lat": "50.873974",
		"long": "0.016394"
	},
	"pub24": {
		"name": "Lewes Arms",
		"address": "1 Mount Place,<br>Lewes,<br>BN7 1YH",
		"phone": "01273 473152",
		"website": "www.thelewesarms.co.uk",
		"lat": "50.873944",
		"long": "0.0095"
	},
	"pub25": {
		"name": "Snowdrop Inn",
		"address": "119 South St, <br />Lewes, <br />East Sussex,<br>BN7 2BU",
		"phone": "01273 471018",
		"website": "www.thesnowdropinn.com",
		"lat": "50.872",
		"long": "0.0229602"
	},
	"pub26": {
		"name": "The Oak",
		"address": "46 St. James's St,<br>Brighton<br>BN2 1RG",
		"phone": "01273 699848",
		"lat": "50.820641",
		"long": "-0.13237"
	},
	"pub27": {
		"name": "Jolly Boatman",
		"address": "133-135 Lewes Rd, <br />Newhaven, <br />East Sussex,<br>BN9 9SJ",
		"phone": "01273 510030",
		"lat": "50.7971",
		"long": "0.0444298"
	},
	"pub28": {
		"name": "Half Moon",
		"address": "Ditchling Rd,<br>Lewes,<br>BN7 3AF",
		"phone": "01273 890253",
		"website": "www.halfmoonplumpton.com/",
		"lat": "50.902376",
		"long": "-0.062437"
	},
	"pub29": {
		"name": "Stanley Arms",
		"address": "47 Wolseley Rd, <br />Portslade, <br />BN41 1SS",
		"phone": "01273 430234",
		"website": "www.thestanley.com/",
		"lat": "50.8373",
		"long": "-0.220018"
	},
	"pub30": {
		"name": "Anchor Inn",
		"address": "Lewes Rd,<br>Ringmer,<br>Lewes,<br>BN8 5QE",
		"phone": "01273 812370",
		"website": "www.anchorinnringmer.co.uk/",
		"lat": "50.893647",
		"long": "0.056357"
	},
	"pub31": {
		"name": "Cock Inn",
		"address": "Uckfield Rd, <br />Ringmer, <br />East Sussex,<br>BN8 5RX",
		"phone": "01273 812040",
		"website": "www.cockpub.co.uk",
		"lat": "50.903972",
		"long": "0.046457"
	},
	"pub32": {
		"name": "Sloop",
		"address": "Sloop Ln,<br>Scaynes Hill,<br>Haywards Heath,<br>West Sussex,<br>RH17 7NP",
		"phone": "01444 831219",
		"website": "www.thesloopinn.com",
		"lat": "51.002117",
		"long": "-0.027687"
	},
	"pub33": {
		"name": "Cinque Ports",
		"address": "49 High St, <br />Bishopstone, <br />Seaford, <br />East Sussex BN25 1PP",
		"phone": "01323 892391",
		"website": "www.cinqueportspub.com",
		"lat": "50.7716",
		"long": "0.104117"
	},
	"pub34": {
		"name": "Old Plough",
		"address": "20 Church Street,<br>Seaford,<br>East Sussex,<br>BN25 1HG",
		"phone": "01323 872921",
		"website": "www.theoldploughseaford.co.uk",
		"lat": "50.771499",
		"long": "0.101061"
	},
	"pub35": {
		"name": "Buckingham Arms",
		"address": "Brunswick Rd, <br />Shoreham-by-Sea, <br />West Sussex <br />BN43 5WA",
		"phone": "01273 453660",
		"lat": "50.8339",
		"long": "-0.272805"
	},
	"pub36": {
		"name": "Duke of Wellington",
		"address": "Brighton Rd, <br />Shoreham-by-Sea, <br />West Sussex <br />BN43 5RE",
		"phone": "01273 389818",
		"website": "www.dukeofwellingtonshoreham.co.uk",
		"lat": "50.83281",
		"long": "-0.2692"
	},
	"pub37": {
		"name": "Red Lion",
		"address": "Old Shoreham Rd,<br />Shoreham-by-Sea,<br />West Sussex <br />BN43 5TE",
		"phone": "01273 453171",
		"website": "www.redlionshoreham.co.uk",
		"lat": "50.84011",
		"long": "-0.28597"
	},
	"pub38": {
		"name": "Horns Lodge",
		"address": "North Chailey,<br /> East Sussex BN8 4BD",
		"phone": "01273 400422",
		"lat": "50.946046",
		"long": "-0.0115021247011"
	},
	"pub39": {
		"name": "The Alma Arms",
		"address": "65 Framfield Road,<br>Uckfield,<br>East Sussex,<br>TN22 5AJ",
		"phone": "01825 762232",
		"website": "www.alma-arms.co.uk",
		"lat": "50.966725",
		"long": "0.101226"
	},
	"pub40": {
		"name": "Cock Inn",
		"address": "North Common Road,<br>Wivelsfield Green,<br>West Sussex,<br>RH17 7RH",
		"phone": "01444 471668",
		"website": "www.cockinn-wivelsfield.co.uk",
		"lat": "50.963329",
		"long": "-0.073127"
	}
}